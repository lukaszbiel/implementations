package javakurs.algorytmy1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CircularQueueTests {
	
	 @Test(expected = IllegalStateException.class)
	    public void should_throw_exception_when_enqueue_and_queue_is_full(){
		 CircularQueue<Integer> buffer = new CircularQueue<Integer>(3);
	        buffer.dequeue(1);
	        buffer.dequeue(2);
	        buffer.dequeue(3);
	       
	    }
	     
	    @Test(expected = IllegalStateException.class)
	    public void should_throw_exception_when_dequeue_and_queue_is_empty(){
	    	CircularQueue<Integer> buffer = new CircularQueue<Integer>(3);
	        buffer.enqueue();
	    }
	     
	    @Test
	    public void queue_works_correctly(){
	    	CircularQueue<Integer> buffer = new CircularQueue<Integer>(3);
	        assertTrue(buffer.isEmpty());
	        assertFalse(buffer.isFull());
	        buffer.dequeue(1);
	        assertFalse(buffer.isEmpty());
	        assertFalse(buffer.isFull());
	        buffer.dequeue(2);
	        assertFalse(buffer.isEmpty());
	        assertTrue(buffer.isFull());
	         
	        assertEquals((Integer) 1, buffer.enqueue());
	        assertEquals((Integer) 2, buffer.enqueue());
	    }

}
