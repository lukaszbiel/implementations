package hashmapimple;

import java.lang.reflect.Array;

public class HashTabImple<K, V> {

	private Entry[] entrys;

	public HashTabImple(int size) {
		entrys = (Entry[]) Array.newInstance(Entry.class, size);
	}

	private class Entry {
		private K key;
		private V value;
		private Entry next;

		public Entry(K key, V value) {
			this.key = key;
			this.value = value;
			this.next = null;
		}

	}

	public V get(K key) {
		Entry tmp;

		int index = key.hashCode() % entrys.length;
		if(entrys[index] == null){
			return null;
		}
		if (entrys[index].next == null) {
			return entrys[index].value;

		} else {
			tmp = entrys[index];
			while (tmp != null &&!tmp.key.equals(key)) {
				tmp = tmp.next;
			}
		}

		return tmp.value;

	}

	public void put(K key, V value) {
		Entry newentry = new Entry(key, value);
		int index = key.hashCode() % entrys.length;
		Entry tmp;
		if (entrys[index] == null) {

			entrys[index] = newentry;
		} else {
			tmp = entrys[index];
			while (tmp.next != null) {
				tmp = tmp.next;
			}
			if(newentry.key.equals(tmp.key)){
				tmp.value = newentry.value;
			}else{
		
			tmp.next = newentry;
			}

		}

	}

	public boolean contains(K key) {
		int index = key.hashCode() % entrys.length;
		if (entrys[index].next == null) {
			if (entrys[index].key == key) {
				return true;
			}
		} else {
			Entry tmp = entrys[index];
			while (tmp.next != null) {
				if (tmp.key.equals(key)) {
					return true;
				}
				return false;
			}
			
		}
		return false;
		
	}
	@SuppressWarnings("unchecked")
	public void resize(int size){
		
		Entry[] oldArray = entrys;
		
		
		entrys = (Entry[]) Array.newInstance(Entry.class, size);
		for(int i =0; i< oldArray.length;i++){
			if(oldArray[i] != null && oldArray[i].next ==null){
				this.put(oldArray[i].key, oldArray[i].value);
			}else if(oldArray[i] != null && oldArray[i].next != null){
				Entry tmp = oldArray[i];
				while(tmp.next != null){
					this.put(tmp.key, tmp.value);
					tmp = tmp.next;
				}
				this.put(tmp.key,tmp.value);
			}
			
			
		}
		
		
	}

	public static void main(String[] args) {
		HashTabImple<String, String> map = new HashTabImple<String, String>(10);
		map.put("ok", "Kasia");
		map.put("ok2", "Marta");
		map.put("ok3", "Asia");
//
		System.out.println(map.get("ok"));
		System.out.println(map.get("ok2"));
		System.out.println(map.get("ok3"));
//		System.out.println(map.contains("ok"));
		map.resize(10);
		System.out.println(map.get("ok2"));
//		map.resize(10);
		
		

	}

}
