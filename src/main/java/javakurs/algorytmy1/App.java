package javakurs.algorytmy1;

import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App {
	
 public static HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
 
 
	public static void main(String[] args) {
		System.out.println(App.fabiarek(4));
	}

	public static int fabiarek(int n) {
		if(map.containsKey(n)){
			return map.get(n);
		}
		if (n < 2)
			return n;
		int fabnumber = fabiarek(n - 1) + fabiarek(n - 2);
		map.put(n, fabnumber);
		return fabnumber;

	}
	
	public static int fabiaire(int n){
		int fib1 =1;
		int fib2 = 1;
		int fibnumber = 1;
		if(n == 1 || n==2){
			return 1;
		}
		for(int i =3; i<n+1;i++){
			fibnumber = fib1 + fib2;
			fib1 = fib2;
			fib2 = fibnumber;
		}
		return fibnumber;
	}
}
	

