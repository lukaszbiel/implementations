package javakurs.algorytmy1;

import java.util.ArrayList;
import java.util.List;

public class DynamicArray<T> {
	
	private   T[] elements;
	private static int firstEmpty;
	static List<String> list = new ArrayList<String>();
	
	
	
	public DynamicArray(int initialSize){
		elements =(T[]) new Object[initialSize];
		firstEmpty = 0;
	}
	
	
	
	public static void  main(String[] args){
		DynamicArray<Integer> tab = new DynamicArray<Integer>(firstEmpty);
		tab.append(1);
		tab.append(2);
		tab.append(3);
//		System.out.println(firstEmpty);
//		tab.instert(1, 5);
		tab.instert(0, 9);
		tab.printArray();
		System.out.println(firstEmpty);
	}
	
	
	public  void extend (){
		int i = 0;
		int size = elements.length;
		T[] newelements = (T[]) new Object[size+1];
		for(T element : elements){
			newelements[i] = element;
			i++;
		}
		elements = newelements;
		
	}
	
	public void append(T element){
		
		if(firstEmpty == elements.length){
			extend();
			
			elements[firstEmpty++] = element;
			
		}else
		elements[firstEmpty] = element;
	}
	public void instert(int index,T element){
		if(firstEmpty == elements.length){
			extend();
			
			for(int i = firstEmpty ;i>index;i--){
				elements[i] = elements[i-1];
			}
			elements[index] = element;
			firstEmpty++;
		}
	}
	
	public void printArray(){
		for(T element : elements){
			System.out.println(element);
		}
	}
	
	public int length(){
		return firstEmpty;
	}
	
	public T get(int index){
		return elements[index];

	}
	
	public void delete(int index){
		for(int i = index;i<firstEmpty-1;i++){
			elements[i] = elements[i+1];
		}
		firstEmpty--;
	}
	
	

}
