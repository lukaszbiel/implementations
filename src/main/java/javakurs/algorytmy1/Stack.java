package javakurs.algorytmy1;

public interface Stack<T> {
	
	public T peek();
	public T pop();
	public void push(T element);
	
	boolean isEmpty();
	
	
	
	
	
	

}
