package javakurs.algorytmy1;

public class LinkedList<T> {

	private Node first;

	private int length;

	public void append(T element) {

		if (first == null) {
			first = new Node(element, null);

		} else {
			Node last = first;
			while (last.next != null) {

				last = last.next;
			}

			Node next = new Node(element, null);
			last.next = next;

		}
		this.length++;
	}

	public void prepend(T element) {
		if (first == null) {
			first = new Node(element, null);
			length++;
		} else {
			Node next = first;

			first = new Node(element, next);
			length++;
		}
	}

	public T get(int index) {
		if (index < 0) {
			return null;
		}
		Node tmpNode = null;

		tmpNode = first;

		for (int i = 0; i < index; i++) {
			tmpNode = tmpNode.next;

		}
		return tmpNode.value;

	}

	public void insert(int index, T element) {
		if (index < 0 || index > length) {
			throw new IndexOutOfBoundsException();

		} else {

			Node tmpNode = first;
			if (index == 0) {
				first = new Node(element, first);
			} else {

				for (int i = 0; i < index - 1; i++) {
					tmpNode = tmpNode.next;

				}
				Node newnode = new Node(element, tmpNode.next);
				tmpNode.next = newnode;

			}
		}
		length++;
	}

	public void delete(int index) {
		if (index < 0 || index > length) {
			throw new IndexOutOfBoundsException();

		} else {
			Node tmpNode = first;
			if (index == 0) {
				first = first.next;

			} else {
				for (int i = 0; i < index - 1; i++) {
					
					tmpNode = tmpNode.next;

				}
				tmpNode.next = tmpNode.next.next;

			}

		}
		length--;
	}
	public void deleteFirst(){
		delete(0);
	}
	public void deleteLast(){
		delete(this.length-1);
	}

	public int length() {
		return this.length;
	}

	private class Node {
		T value;
		Node next;

		public Node(T value, Node next) {
			this.value = value;
			this.next = next;
		}
	}

}
