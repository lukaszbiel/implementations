package javakurs.algorytmy1;

public class CircularQueue<T> {

	private int index;
	private int nextReader;
	private int nextWriter;
	private T[] elements;

	public CircularQueue(int index) {

		nextReader = 0;
		nextWriter = 0;
		elements = (T[]) new Object[index];
	}

	public T enqueue() {
		if (nextWriter == 0 && nextReader == 0) {
			throw new IllegalStateException();
		}

		T element = elements[nextReader];
		elements[nextReader] = null;
		nextReader++;
		return element;

	}

	public void dequeue(T element) {
		if (isFull()) {
			throw new IllegalStateException();
		}else {
			elements[nextWriter] = element;
			
			nextWriter++;
			
		}

		

	}

	public boolean isFull() {
		if (nextWriter == nextReader -1 || nextReader ==0 && nextWriter == elements.length-1) {
			return true;
		}
		return false;

	}

	public boolean isEmpty() {
		if (nextWriter == nextReader) {
			return true;
		}
		return false;

	}

	public static void main(String[] args) {

		CircularQueue<Integer> list = new CircularQueue<Integer>(3);

		list.dequeue(19);
		System.out.println("nextWriter " + list.nextWriter + " nextReader" + list.nextReader);
		list.dequeue(22);
		System.out.println("nextWriter " + list.nextWriter + " nextReader" + list.nextReader);
		list.dequeue(23);
		System.out.println("nextWriter " + list.nextWriter + " nextReader" + list.nextReader);

		// System.out.println(list.nextWriter + " nextReader "
		// +list.nextReader);

		// list.dequeue(22);

		// list.dequeue(23);
		// System.out.println("nextWriter " + list.nextWriter + " nextReader"
		// +list.nextReader);
		// System.out.println(list.enqueue() + " nextReader = " +
		// list.nextReader);
		// System.out.println(list.enqueue() + " nextReader = " +
		// list.nextReader);
		// System.out.println(list.enqueue() + " nextReader = " +
		// list.nextReader);
		// System.out.println(list.enqueue() + " nextReader = " +
		// list.nextReader);
		// System.out.println(list.enqueue() + " nextReader = " +
		// list.nextReader);

	}

}
