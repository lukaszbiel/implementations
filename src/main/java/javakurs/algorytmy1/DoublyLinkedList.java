package javakurs.algorytmy1;

public class DoublyLinkedList<T> {
	private int length;
	private Node first;
	private Node last;

	private class Node {

		T value;

		public Node(T value, Node previous, Node next) {

			this.value = value;
			this.previous = previous;
			this.next = next;
		}

		Node previous;
		Node next;
	}

	public static void main(String[] args) {

		DoublyLinkedList<Integer> newList = new DoublyLinkedList<Integer>();

		 newList.append(2);
		 newList.append(3);
		 newList.append(5);
		 newList.print();
		// newList.prepend(6);
		 newList.insert(5, 4);
		// newList.insert(0, 3);
//		 System.out.println("length "+newList.length);
		 newList.print();
//		newList.insert(0, 5);
//		newList.print();
//		 System.out.println("length "+newList.length);
//		newList.insert(0, 3);
//	


		// System.out.println(newList.length);
		// System.out.println("del " + newList.delete(2));
//		newList.print();
		// System.out.println(newList.length);
		// System.out.println("Get 0 = " + newList.get(0));
		// System.out.println("Get 1 = " + newList.get(1));
		// System.out.println("Get 2 = " + newList.get(2));

	}

	public void print() {

		System.out.print("Nodes Forward: ");
		Node tmpNode;

		tmpNode = first;
		while (tmpNode != null) {
			System.out.print(" " + tmpNode.value + " ");
			tmpNode = tmpNode.next;
		}
		System.out.println("");

		System.out.print("Nodes Backward: ");
		tmpNode = last;
		while (tmpNode != null) {
			System.out.print(" " + tmpNode.value + " ");
			tmpNode = tmpNode.previous;
		}
		System.out.println("");

	}

	public void append(T element) {
		if (length == 0) {
			Node tmpNode = new Node(element, null, null);
			this.last = tmpNode;
			this.first = tmpNode;

		} else {
			Node tmpNode = null;
			tmpNode = this.last;
			Node newNode = new Node(element, tmpNode, null);
			tmpNode.next = newNode;
			this.last = newNode;

		}
		this.length++;
	}

	public int length() {
		return length;
	}

	public T get(int index) {

		if (index < 0) {
			return null;
		}
		Node tmpNode = null;

		tmpNode = this.first;

		for (int i = 0; i < index; i++) {
			tmpNode = tmpNode.next;

		}
		return tmpNode.value;

	}

	private Node getNode(int index) {

		if (index < 0) {
			return null;
		}
		Node tmpNode = null;

		tmpNode = this.first;

		for (int i = 0; i < index; i++) {
			tmpNode = tmpNode.next;

		}
		return tmpNode;

	}

	public void deleteFirst() {

		delete(0);

	}

	public void deleteLast() {
		delete(length - 1);
	}

	public T delete(int index) {
		Node deleteNode = null;
		if (index == 0) {

			deleteNode = first;
			if (this.last == this.first) {
				this.first = null;
				this.last = null;

			} else {
				this.first.next.previous = null;
				this.first = this.first.next;
			}

		} else if (index == length - 1) {
			deleteNode = getNode(length - 1);
			deleteNode.previous.next = null;
			this.last = deleteNode.previous;

		} else {
			deleteNode = getNode(index);
			deleteNode.previous.next = deleteNode.next;
			deleteNode.next.previous = deleteNode.previous;

		}
		length--;
		return deleteNode.value;
	}

	public void insert(int index, T element) {
		if (index < 0 || index > length) {
			throw new IndexOutOfBoundsException();

		}
		if (index == 0) {
			prepend(element);
		} else if (index == length) {
			append(element);
		} else {
			Node nodeindex = getNode(index - 1);
			Node nodetoInsert = new Node(element, nodeindex, nodeindex.next);
			nodeindex.next.previous = nodetoInsert;
			nodeindex.next = nodetoInsert;
			length++;
		}

	}

	public void prepend(T element) {
		if (length == 0) {
			Node tmpNode = new Node(element, null, null);
			this.last = tmpNode;
			this.first = tmpNode;

		} else {
			Node tmpNode = new Node(element, null, first);
			tmpNode.next.previous = tmpNode;
			first = tmpNode;

		}
		this.length++;
	}
}
